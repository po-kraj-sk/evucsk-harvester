[evucsk]
db = postgresql+psycopg://$DB_CONNECTION_STRING
db_schema = $DB_SCHEMA

user=$EVUCSK_USERNAME
password=$EVUCSK_PASSWORD

be_careful = $BE_CAREFUL
# 1 day, in seconds
#cache_expire = 86400


[lekarne]
url=https://rhf.e-vuc.sk/_api_export_lekaren.jsp?mandant=$EVUCSK_ID
git_repo=$GIT_LOCAL_FOLDER
#local_dataset_fn=lekarne_pharm.xml
enabled=$EVUCSK_ENABLE_LEKARNE


[pohotovost]
url=https://rhf.e-vuc.sk/_api_export_pohotovost.jsp?mandant=$EVUCSK_ID
git_repo=$GIT_LOCAL_FOLDER
#local_dataset_fn=lekarne_pohotovost.xml
enabled=$EVUCSK_ENABLE_POHOTOVOST


[zariadenia]
url=https://rz.e-vuc.sk/_api_export.jsp?mandant=$EVUCSK_ID
git_repo=$GIT_LOCAL_FOLDER
#local_dataset_fn=zariadenia.xml
enabled=$EVUCSK_ENABLE_ZARIADENIA


[socialne_sluzby]
url=https://rsp.e-vuc.sk/_api_export.jsp?mandant=$EVUCSK_ID
git_repo=$GIT_LOCAL_FOLDER
#local_dataset_fn=socialne_sluzby.xml
enabled=$EVUCSK_ENABLE_SLUZBY

[requests]
retry_connect=$RETRY_CONNECT
retry_backoff=$RETRY_BACKOFF

[sqlalchemy]
echo = False

#[prometheus]
#uri = %(PROM_PUSHGATEWAY_URI)s
