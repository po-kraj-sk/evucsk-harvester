-- It may not be easy to utilize "raw data" (i.e. data in structure
-- provided by eVuc.sk) in apps or reports. In such cases, we need to
-- implement also some "2nd ETL stage" (after harvesting). This 2nd stage can
-- be whatever:
--
--	SQL statement
--	yet another Python script, possibly utilizing Pandas
--	Talend job
--	etc.
--
-- Here we provide several examples utilizing views.
--
-- Note: Apart from correctness, examples aim more for "easy to read" and
-- less for "quick/efficient operation".
--
--
-- ### lekarne
--
-- This provides general overview of all pharmacies ('lekarne') but also
-- contact information ('kontaky') and opening hours ('prevadzkovy_cas' a.k.a.
-- 'pc') is included.
--
-- Main view is 'pivoted_lekarne', the rest is "support" for simplification.

CREATE OR REPLACE VIEW evucsk.pivoted_lekarne_kontakty AS
SELECT
	lekaren_id,
	string_agg(hodnota, ', ' ORDER BY index) FILTER (WHERE typ = 'EMAIL') AS email,
	string_agg(hodnota, ', ' ORDER BY index) FILTER (WHERE typ = 'MOBIL') AS mobil,
	string_agg(hodnota, ', ' ORDER BY index) FILTER (WHERE typ = 'TELEFON') AS telefon
FROM
	evucsk.lekarne_kontakty
GROUP BY
	lekaren_id;


CREATE OR REPLACE VIEW evucsk.pivoted_lekarne_otvaracie_hodiny AS
WITH
	raw_od_do AS (
		SELECT
			usek.id AS usek_id, pc.lekaren_id, translate(lower(dni.nazov_dna), 'šľ', 'sl') AS nazov_dna, (usek.od || ' - ' || usek.do) AS od_do
		FROM
			evucsk.lekarne_prevadzkovy_cas_usek_dna AS usek,
			evucsk.lekarne_prevadzkovy_cas_dni AS dni,
			evucsk.lekarne_prevadzkovy_cas AS pc
		WHERE
			usek.den_id = dni.id AND
			dni.pc_id = pc.id
	)
SELECT
	lekaren_id,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'pondelok') AS pondelok,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'utorok') AS utorok,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'streda') AS streda,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'stvrtok') AS stvrtok,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'piatok') AS piatok,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'sobota') AS sobota,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'nedela') AS nedela
FROM
	raw_od_do
GROUP BY
	lekaren_id;


CREATE OR REPLACE VIEW evucsk.pivoted_lekarne AS
SELECT
	l.lekaren_id,
	l.identifikator,
	l.kpzs_kod,
	l.nazov,
	l.typ_lekarne,
	l.poloha_lat,
	l.poloha_lon,
	l.odborny_zastupca,
	l.datum_zacatia_cinnosti,
	l.addressline,
	l.municipality,
	l.buildingnumber,
	l.county,
	l.streetname,
	l.unit,
	l.postalcode,
	l.prevadzkovatel_id,
	k.email,
	k.mobil,
	k.telefon,
	oh.pondelok,
	oh.utorok,
	oh.streda,
	oh.stvrtok,
	oh.piatok,
	oh.sobota,
	oh.nedela
FROM evucsk.lekarne l
LEFT JOIN evucsk.pivoted_lekarne_kontakty k ON (l.lekaren_id = k.lekaren_id)
LEFT JOIN evucsk.pivoted_lekarne_otvaracie_hodiny oh ON (l.lekaren_id = oh.lekaren_id);


-- ### soc_sluzby
--
-- This provides general overview of all social services ('soc_sluzby') but also
-- contact information ('kontaky').
--
-- Main view is 'pivoted_soc_sluzby', the rest is "support" for simplification.

CREATE OR REPLACE VIEW evucsk.pivoted_soc_sluzby_kontakty AS
SELECT
	sluzba_id,
	string_agg(hodnota, ', ' ORDER BY index) FILTER (WHERE typ = 'EMAIL') AS email,
	string_agg(hodnota, ', ' ORDER BY index) FILTER (WHERE typ = 'MOBIL') AS mobil,
	string_agg(hodnota, ', ' ORDER BY index) FILTER (WHERE typ = 'TELEFON') AS telefon
FROM
	evucsk.soc_sluzby_kontakty
GROUP BY
	sluzba_id;


CREATE OR REPLACE VIEW evucsk.pivoted_soc_sluzby AS
SELECT
	s.id,
	s.poskytovatel_id,
	p.ico,
	p.corporatebodyfullname,
	p.typ_poskytovatela,
	p.semafor,
	s.cielova_skupina,
	s.druh_socialnej_sluzby,
	s.druh_socialnej_sluzby_poznamka,
	s.forma_socialnej_sluzby,
	s.den_zacatia_poskytovania,
	s.den_ukoncenia_poskytovania,
	s.doplnujuca_informacia,
	s.info_na_web,
	s.kapacita,
	s.kapacita_zien,
	s.rozsah_socialnej_sluzby,
	s.poloha_lat,
	s.poloha_lon,
	s.vypocitana_poloha,
	s.web,
	s.addressline,
	s.municipality,
	s.buildingname,
	s.buildingnumber,
	s.county,
	s.propertyregistrationnumber,
	s.streetname,
	s.unit,
	s.postalcode,
	s.druh_socialnej_sluzby_kod,
	s.obec,
	s.okres,
	s.kraj,
	k.email,
	k.mobil,
	k.telefon
FROM evucsk.soc_sluzby s
LEFT JOIN evucsk.pivoted_soc_sluzby_kontakty k ON k.sluzba_id = s.id
LEFT JOIN evucsk.soc_sluzby_poskytovatelia p ON p.id = s.poskytovatel_id;


-- ### zariadenia
--
-- This provides general overview of all facilities ('zariadenia') but also
-- contact information ('kontaky') and opening hours ('prevadzkovy_cas' a.k.a.
-- 'pc') is included.
--
-- Main view is 'pivoted_zariadenia', the rest is "support" for simplification.

CREATE OR REPLACE VIEW evucsk.pivoted_zariadenia_kontakty AS
SELECT
	zariadenie_id,
	string_agg(hodnota, ', ' ORDER BY index) FILTER (WHERE typ = 'EMAIL') AS email,
	string_agg(hodnota, ', ' ORDER BY index) FILTER (WHERE typ = 'MOBIL') AS mobil,
	string_agg(hodnota, ', ' ORDER BY index) FILTER (WHERE typ = 'TELEFON') AS telefon
FROM
	evucsk.zariadenia_kontakty
GROUP BY
	zariadenie_id;


CREATE OR REPLACE VIEW evucsk.pivoted_zariadenia_odborni_zastupcovia AS
SELECT
	zariadenie_id,
	string_agg(meno, ', ' ORDER BY id) AS odborni_zastupcovia
FROM
	evucsk.zariadenia_odborni_zastupcovia
GROUP BY
	zariadenie_id;


CREATE OR REPLACE VIEW evucsk.pivoted_zariadenia_otvaracie_hodiny AS
WITH
	raw_od_do AS (
		SELECT
			usek.id AS usek_id, pc.zariadenie_id, translate(lower(dni.nazov_dna), 'šľ', 'sl') AS nazov_dna, (usek.od || ' - ' || usek.do) AS od_do
		FROM
			evucsk.zariadenia_prevadzkovy_cas_usek_dna AS usek,
			evucsk.zariadenia_prevadzkovy_cas_dni AS dni,
			evucsk.zariadenia_prevadzkovy_cas AS pc
		WHERE
			usek.den_id = dni.id AND
			dni.pc_id = pc.id
	)
SELECT
	zariadenie_id,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'pondelok') AS pondelok,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'utorok') AS utorok,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'streda') AS streda,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'stvrtok') AS stvrtok,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'piatok') AS piatok,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'sobota') AS sobota,
	string_agg(od_do, ', ' ORDER BY usek_id) FILTER (WHERE nazov_dna = 'nedela') AS nedela
FROM
	raw_od_do
GROUP BY
	zariadenie_id;


CREATE OR REPLACE VIEW evucsk.pivoted_zariadenia AS
SELECT
	z.id,
	z.identifikator,
	z.kpzs_kod,
	z.druh_zariadenia,
	z.nazov_zariadenia,
	z.typ_zariadenia_id,
	z.typ_zariadenia_complex_code,
	z.poloha_lat,
	z.poloha_lon,
	z.addressline,
	z.municipality,
	z.buildingnumber,
	z.county,
	z.streetname,
	z.postalcode,
	z.druh_zariadenia_kod,
	z.poskytovatel_id,
	z.buildingname,
	z.unit,
	k.email,
	k.mobil,
	k.telefon,
	oz.odborni_zastupcovia,
	oh.pondelok,
	oh.utorok,
	oh.streda,
	oh.stvrtok,
	oh.piatok,
	oh.sobota,
	oh.nedela,
	n.od AS nepritomnost_od,
	n.do AS nepritomnost_do,
	n.info,
	n.zastup_v_ambulancii,
	z.poistovne,
	CASE
		WHEN (z.poistovne ~~ '%0%'::text) THEN 'áno'::text
		ELSE 'nie'::text
	END AS "union",
	CASE
		WHEN (z.poistovne ~~ '%1%'::text) THEN 'áno'::text
		ELSE 'nie'::text
	END AS vszp,
	CASE
		WHEN (z.poistovne ~~ '%2%'::text) THEN 'áno'::text
		ELSE 'nie'::text
	END AS dovera
FROM evucsk.zariadenia z
LEFT JOIN evucsk.zariadenia_nepritomnosti n ON (z.id = n.zariadenie_id)
LEFT JOIN evucsk.pivoted_zariadenia_kontakty k ON (z.id = k.zariadenie_id)
LEFT JOIN evucsk.pivoted_zariadenia_odborni_zastupcovia oz ON (z.id = oz.zariadenie_id)
LEFT JOIN evucsk.pivoted_zariadenia_otvaracie_hodiny oh ON (z.id = oh.zariadenie_id);
