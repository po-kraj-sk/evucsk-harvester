# Intro

Several self-governing regions (a.k.a. VÚC, in Slovak) are using services of
eVuc.sk to manage some their agenda. While managing that agenda, data is
created and updated by VÚC staff in database(s) of eVuc.sk. In order to
further facilitate use of this data within a VÚC, following is needed:

1) ability to get the data from eVuc.sk
2) ability to periodically harvest that data and push it to other
   databases, for example database use in geopresovregion.sk portal

eVuc.sk is providing API to cover first point. This harvester is implementing
second point.

Harvester (as an ETL tool) is downloading files provided via API by eVuc.sk,
Extracting required information and storing (Loading) it in PostgreSQL database.
To keep the harvester simple and efficient, Transformations are kept at minimum.

Harvester is supposed to be run periodically, from the `cron`, once a day
(or as often as needed, considering how often the data gets updated),
preferably during the night.


## About eVuc.sk data

eVuc.sk API consists of set of URLs, each providing data for certain
topic/agenda (dataset) as an XML file. Each dataset is quite richly
structured, hence data is decomposed to several DB tables.

Since some portion of the data are intended only for internal use by a  VÚC
and some data contains personal information, URLs of the API require
authentication to make sure only authorized personnel gets access to the data.

There is no "last modification time" available within HTTP response or data
itself, hence harvesting is "brute force": each run means complete ETL,
updating even unchanged data in the DB.

## About access to eVuc.sk API

You need to be their customer to get access to your data stared in there.
Or you need have a cooperation with their customer based on which their
customer will get you the credentials needed to use the API.

Credentials consist of user name and password which you will need to put
into the configuration file, see bellow (section "How to install").


# License

This code is licensed under the [EUPL](LICENSE.txt).


# Requirements

Python 3.11 or newer


# How to install

1. checkout from Git: https://gitlab.com/po-kraj-sk/evucsk-harvester.git
2. create virtual environment: `virtualenv --python=python3.11 .venv`
3. activate: `source .venv/bin/activate`
4. install dependencies: `pip install -r requirements.txt`
5. create configuration file: `cp evucsk_harvester.conf.example evucsk_harvester.conf`

Then also check:

6. that database exists along with schema 'evucsk'
7. user running harvester is able to create tables in the database schema
8. configuration in `evucsk_harvester.conf`, adjust at minimum at least following items:
   * `db_name`
   * `user`
   * `password`
   * `id` in 'url' setting in each configuration section


# How to run

From command-line:

`python -m evucsk_harvester`


From `cron` (output will be directed to `evucsk_harvester.log` file):

`evucsk_harvester.sh`


## be_careful setting

When `True`, harvester will try avoiding overloading either source server
or target database.

Following will be done to avoid overloading source server:

1. server responses will be kept in local SQlite cache
2. after getting data from server, small sleep will be done


## Updates of dataset schema

As of now, code does not handle changes in dataset structure (i.e. DB
schema updates).

If the structure changes, DB (or affected tables) needs to be dropped and
all data downloaded again.

Or DB schema (table names and structure) needs to be updated by hand.



# Using Docker
A Dockerfile and a docker-compose are available. The docker-compose provides an
easy way to build and test the docker image and the harvesting service. You will need, though, to get credentials for the service and set thme for the following environment variables in the docker-compose:
```
      EVUCSK_USERNAME: "evucdatauser"
      EVUCSK_PASSWORD: "evucdataverysecretpassword"
      EVUCSK_ID: "evucdataid"
```


To see the harvester in action:
    # build the image
    docker-compose build
    # run it
    docker-compose up -d
    # follow the logs
    docker-compose logs -f

You can see the results in the associated database, on port 5433 (use your
favorite tool for database inspection).

The docker-compose.yml file shows you how to configure the docker container
using environment variables. The list of available environment variables can be
seen in the Dockerfile.

## Logs persistence
You are expected to bind a volume to `/app/logs` folder. If bind-mounting to a FS folder, the chown command from the Dockerfile will not  operate: you will have to manually create and chown the volume folder to uid 999


# Information for developers

Branches, pull-requests, releases, etc.: according to [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/) but ...

... in order to allow better overview for current and future contributors, we'd like to conclude features with pull-requests,
i.e. instead of `git flow feature finish MYFEATURE` do following:

1. `git flow feature publish MYFEATURE`
2. go to GitLab and open pull-request from your feature branch to `develop`
3. review + adjustments
4. merge + delete branch after merge


## Git hooks

For source code formatting, etc.:

`pre-commit install`
