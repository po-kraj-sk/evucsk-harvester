from .lekarne import LekarneHarvester
from .pohotovost import PohotovostHarvester
from .socialne_sluzby import SocialneSluzbyHarvester
from .zariadenia import ZariadeniaHarvester
