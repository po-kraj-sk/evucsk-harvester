# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl 5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# General notes about how the XML data is processed:
#
# 1) There is 1-N relationship between 'prevadzkovatel' and 'lekaren'
# so we're splitting those in two tables.
#
# 2) To link them together, we're using 'id' from the supplied data.
#
# 3) It might be a good idea to split into separate DB table also addresses,
# but we are not doing that, mainly because those entries do not have ID.
# And if we derive ID ourselves, it would depend on ordering, which may
# change and thus may complicate thing later on. Plus addresses are also
# under 'zariadenia' dataset and we do not know whether both are coming from
# same DB (thus we might be able to rely on some consistency) or not.
#
# 4) Similarly as in (1), 'lekaren' can have 0-N 'kontakt' thus splitting
# that in yet another table.
#
# 5) 'prevadzkovy_cas' similarly as 'kontakt' - point (4) - but is more
# complicated, as it further decomposes to 'den' and 'usek_dna'
#
#
# Terminology:
# - prevadzkovatel: entity running one or more pharmacies
# - lekaren: pharmacy
# - kontakt: contact entry for a pharmacy
# - prevadzkovy_cas: opening hours
# - den: day
# - usek dna: part of the day

import logging
from xml.etree import ElementTree

from pandas import DataFrame
import sqlalchemy

from .common import HarvesterBase


logger = logging.getLogger()


class LekarneHarvester(HarvesterBase):

    CONFIG_SECTION = "lekarne"

    TN_PREVADZKOVATEL = "lekarne_prevadzkovatel"
    TN_LEKARNE = "lekarne"
    TN_KONTAKTY = "lekarne_kontakty"
    TN_PC = "lekarne_prevadzkovy_cas"
    TN_PC_DNI = "lekarne_prevadzkovy_cas_dni"
    TN_PC_USEK_DNA = "lekarne_prevadzkovy_cas_usek_dna"
    TN_PREKAZKA = "lekarne_prekazky"

    KONTAKT_BACKREFERENCE_KEY = "lekaren_id"
    # PC IDs in 'lekaren' are filled OK => we'll use them
    # (note: This is 'False' in 'ZariadeniaHarvester, which is re-using code from here.)
    USE_PC_ID_FROM_DATASET = True

    def __init__(self, cfg_parser, common_resources):
        super().__init__(cfg_parser, common_resources)
        self.prevadzkovatel_list = None
        self.lekaren_list = None
        self.kontakt_list = None
        self.pc_list = self.pc_id_counter = None  # 'pc' = "prevadzkovy cas"
        self.pc_den_list = self.pc_den_id_counter = None  # 'pc' has days ('den')
        self.pc_usek_dna_list = (
            self.pc_usek_dna_id_counter
        ) = None  # 'den' has parts of day ('usek_dna'()
        self.prekazka_list = None

    def parse_usek_dna(self, xml_element, den_id):
        usek_dna = {}

        self.parse_elements(xml_element, usek_dna, ["OD", "DO"])
        usek_dna["den_id"] = den_id

        # no ID -> invent/assign some
        usek_dna["id"] = self.pc_usek_dna_id_counter
        self.pc_usek_dna_id_counter += 1

        return usek_dna

    def parse_den(self, xml_element, pc_id):
        den = {}

        self.parse_elements(xml_element, den, ["NAZOV_DNA"])
        self.parse_elements(xml_element, den, ["POZNAMKA_DNA"], mandatory=False)
        den["pc_id"] = pc_id

        # no ID -> invent/assign some
        den["id"] = self.pc_den_id_counter
        self.pc_den_id_counter += 1

        for xml_usek_dna in xml_element.findall("USEK_DNA"):
            usek_dna = self.parse_usek_dna(xml_usek_dna, den["id"])
            self.pc_usek_dna_list.append(usek_dna)

        return den

    def parse_prevadzkovy_cas(self, xml_element, lekaren_id):
        prevadzkovy_cas = {}

        self.parse_elements(
            xml_element,
            prevadzkovy_cas,
            ["ID", "POZNAMKA", "NAZOV", "POPLATOK", "PLATNE_OD", "PLATNE_DO"],
        )
        prevadzkovy_cas[self.KONTAKT_BACKREFERENCE_KEY] = lekaren_id

        # type conversions
        if self.USE_PC_ID_FROM_DATASET:
            self.apply_conversion(prevadzkovy_cas, ["id"], self.convert_int)
        else:
            prevadzkovy_cas["id"] = self.pc_id_counter
            self.pc_id_counter += 1
        self.apply_conversion(
            prevadzkovy_cas, ["platne_od", "platne_do"], self.convert_date
        )

        for xml_den in xml_element.find("DNI").findall("DEN"):
            den = self.parse_den(xml_den, prevadzkovy_cas["id"])
            self.pc_den_list.append(den)

        return prevadzkovy_cas

    def parse_prevadzkove_casy(self, xml_element, lekaren_id):
        # opening hours (prevadzkove_casy), optional
        xml_prevadzkove_casy = xml_element.find("PREVADZKOVE_CASY")
        if xml_prevadzkove_casy:
            for xml_kontakt in xml_prevadzkove_casy.findall("PREVADZKOVY_CAS"):
                prevadzkovy_cas = self.parse_prevadzkovy_cas(xml_kontakt, lekaren_id)
                self.pc_list.append(prevadzkovy_cas)

    def parse_prekazka(self, xml_element, lekaren_id):
        prekazka = {}

        self.parse_elements(xml_element, prekazka, ["OD", "DO", "INFO"])
        self.parse_elements(xml_element, prekazka, ["DOCASNE"], mandatory=False)
        prekazka[self.KONTAKT_BACKREFERENCE_KEY] = lekaren_id

        # type conversions
        self.apply_conversion(prekazka, ["od", "do"], self.convert_date)
        self.apply_conversion(prekazka, ["docasne"], self.convert_bool)

        return prekazka

    def parse_prekazky(self, xml_element, lekaren_id):
        # barriers (prekazky), optional
        xml_prekazky = xml_element.find("PREKAZKY")
        if xml_prekazky:
            for xml_kontakt in xml_prekazky.findall("PREKAZKA"):
                prekazka = self.parse_prekazka(xml_kontakt, lekaren_id)
                self.prekazka_list.append(prekazka)

    def parse_lekaren(self, xml_element):
        """
        parse XML record of one pharmacy
        """
        lekaren = {}
        self.parse_elements(
            xml_element,
            lekaren,
            [
                "LEKAREN_ID",
                "IDENTIFIKATOR",
                "NAZOV",
                "TYP_LEKARNE",
                "POLOHA_LAT",
                "POLOHA_LON",
                "VYPOCITANA_POLOHA",
            ],
        )
        self.parse_elements(
            xml_element,
            lekaren,
            ["ODBORNY_ZASTUPCA", "DATUM_ZACATIA_CINNOSTI"],
            mandatory=False,
        )
        self.parse_physical_address(xml_element, lekaren)
        # sort of outlier: well, sometimes KPZS is missing
        xml_kpzs_kod = xml_element.find("KPZS")
        if xml_kpzs_kod:
            lekaren["kpzs_kod"] = xml_kpzs_kod.find("KOD").text
        else:
            logger.debug("no KPZS available for lekaren: %s" % lekaren["lekaren_id"])
            lekaren["kpzs_kod"] = None

        # type conversions
        lekaren_id = self.convert_int(lekaren["lekaren_id"])
        lekaren["lekaren_id"] = lekaren_id
        self.apply_conversion(lekaren, ["vypocitana_poloha"], self.convert_bool)
        self.apply_conversion(
            lekaren, ["poloha_lat", "poloha_lon"], self.convert_lat_lon
        )

        # other nested stuff:
        self.parse_kontakty(xml_element, lekaren_id)
        self.parse_prevadzkove_casy(xml_element, lekaren_id)
        self.parse_prekazky(xml_element, lekaren_id)

        return lekaren

    def parse_lekarne(self, xml_element, prevadzkovatel):
        """
        parse list of pharmacies in given XML element
        """

        lekarne = []
        for xml_lekaren in xml_element.findall("LEKAREN"):
            lekaren = self.parse_lekaren(xml_lekaren)
            lekaren["prevadzkovatel_id"] = prevadzkovatel["id"]
            lekarne.append(lekaren)

        return lekarne

    def parse_prevadzkovatel(self, xml_element):
        prevadzkovatel = {}
        self.parse_elements(
            xml_element, prevadzkovatel, ["ID", "CorporateBodyFullName", "ICO"]
        )
        logger.debug("processing prevadzkovatel: %s" % prevadzkovatel["id"])

        # type conversions
        self.apply_conversion(prevadzkovatel, ["id"], self.convert_int)

        # well, sometimes address is missing
        self.parse_physical_address(xml_element, prevadzkovatel, mandatory=False)

        return prevadzkovatel

    def parse_povolenie(self, xml_element, prevadzkovatel):
        self.parse_elements(xml_element, prevadzkovatel, ["CISLO_ROZHODNUTIA"])

    def parse_row(self, xml_row):
        prevadzkovatel = self.parse_prevadzkovatel(xml_row.find("PREVADZKOVATEL"))
        self.parse_povolenie(xml_row.find("POVOLENIE"), prevadzkovatel)
        lekaren_list = self.parse_lekarne(xml_row.find("LEKARNE"), prevadzkovatel)
        # 'lekaren_list' is "for this particular prevadzkovatel"

        self.prevadzkovatel_list.append(prevadzkovatel)
        self.lekaren_list.extend(lekaren_list)

    def process_dataset(self, xml_data):
        # we will be working with those dicts throughout
        # this "parsing session" (= one run of `run()`)
        self.prevadzkovatel_list = []
        self.lekaren_list = []
        self.kontakt_list = []
        self.pc_list = []
        self.pc_den_list = []
        self.pc_usek_dna_list = []
        self.pc_id_counter = self.pc_den_id_counter = self.pc_usek_dna_id_counter = 0
        self.prekazka_list = []

        xml_root = ElementTree.fromstring(xml_data)
        for row in xml_root.findall("ROW"):
            self.parse_row(row)

    def store_df_pc(self, name_pc, name_pc_den, name_pc_usek_dna):
        df_pc = DataFrame(self.pc_list)
        df_pc.set_index("id", append=False, inplace=True, verify_integrity=True)
        df_pc.set_index(
            self.KONTAKT_BACKREFERENCE_KEY,
            append=True,
            inplace=True,
            verify_integrity=True,
        )
        # Some fields are optional and sometimes "all empty", then data type
        # will not be guessed properly, hence 'text' will be tried which will
        # cause problems like for example issue #11. So we help a little:
        df_pc_dtype = {"platne_do": sqlalchemy.Date()}
        self.store_table(df_pc, name_pc, dtype=df_pc_dtype)
        self.pc_list = df_pc = None

        df_pc_den = DataFrame(self.pc_den_list)
        df_pc_den.set_index("id", append=False, inplace=True, verify_integrity=True)
        df_pc_den.set_index("pc_id", append=True, inplace=True, verify_integrity=True)
        self.store_table(df_pc_den, name_pc_den)
        self.pc_den_list = df_pc_den = None

        df_pc_usek_dna = DataFrame(self.pc_usek_dna_list)
        df_pc_usek_dna.set_index(
            "id", append=False, inplace=True, verify_integrity=True
        )
        df_pc_usek_dna.set_index(
            "den_id", append=True, inplace=True, verify_integrity=True
        )
        self.store_table(df_pc_usek_dna, name_pc_usek_dna)
        self.pc_usek_dna_list = df_pc_usek_dna = None

    def store_dataset(self):
        # convert lists to data frames and store them in DB
        df_prevadzkovatel = DataFrame(self.prevadzkovatel_list)
        df_prevadzkovatel.set_index(
            "id", append=False, inplace=True, verify_integrity=True
        )
        df_prevadzkovatel.set_index(
            "ico", append=True, inplace=True, verify_integrity=True
        )
        self.store_table(df_prevadzkovatel, self.TN_PREVADZKOVATEL)
        self.prevadzkovatel_list = df_prevadzkovatel = None

        df_lekaren = DataFrame(self.lekaren_list)
        df_lekaren.set_index(
            self.KONTAKT_BACKREFERENCE_KEY,
            append=False,
            inplace=True,
            verify_integrity=True,
        )
        df_lekaren.set_index(
            ["identifikator", "kpzs_kod"],
            append=True,
            inplace=True,
            verify_integrity=True,
        )
        self.store_table(df_lekaren, self.TN_LEKARNE)
        self.lekaren_list = df_lekaren = None

        df_prekazka = DataFrame(self.prekazka_list)
        df_prekazka.rename_axis("id", inplace=True)
        df_prekazka.set_index(
            self.KONTAKT_BACKREFERENCE_KEY,
            append=True,
            inplace=True,
            verify_integrity=True,
        )
        self.store_table(df_prekazka, self.TN_PREKAZKA)
        self.prekazka_list = df_prekazka = None

        # following are in extra methods, since re-used is 'ZariadeniaHarvester'
        self.store_df_kontakty(self.TN_KONTAKTY)
        self.store_df_pc(self.TN_PC, self.TN_PC_DNI, self.TN_PC_USEK_DNA)
