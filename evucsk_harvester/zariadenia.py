# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl 5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# General notes about how the XML data is processed:
#
# 1) There is 1-N relationship between 'poskytovatel' and 'zariadenie'
# so we're splitting those in two tables.
#
# 2) To link them together, we're using 'id' from the supplied data.
#
# 3) It might be a good idea to split into separate DB table also addresses,
# but we are not doing that, mainly because those entries do not have ID.
# And if we derive ID ourselves, it would depend on ordering, which may
# change and thus may complicate thing later on. Plus addresses are also
# under 'lekarne' dataset and we do not know whether both are coming from
# same DB (thus we might be able to rely on some consistency) or not.
#
#
# Terminology:
# - poskytovatel: entity running one or more facilities
# - zariadenie: facility (health, etc.)

import logging
from xml.etree import ElementTree

from pandas import DataFrame
import sqlalchemy

from .lekarne import LekarneHarvester


logger = logging.getLogger()


class ZariadeniaHarvester(LekarneHarvester):

    CONFIG_SECTION = "zariadenia"

    TN_KONTAKTY = "zariadenia_kontakty"
    TN_NEPRITOMNOSTI = "zariadenia_nepritomnosti"
    TN_ODBORY = "zariadenia_odbory"
    TN_ODBORY_UPS = "zariadenia_odbory_ups"
    TN_PC = "zariadenia_prevadzkovy_cas"
    TN_PC_DNI = "zariadenia_prevadzkovy_cas_dni"
    TN_PC_USEK_DNA = "zariadenia_prevadzkovy_cas_usek_dna"
    TN_POSKYTOVATEL = "zariadenia_poskytovatel"
    TN_POISTOVNE = "zariadenia_poistovne"
    TN_ROZPIS = "zariadenia_rozpis"
    TN_SLUZBA = "zariadenia_rozpis_sluzba"
    TN_ZARIADENIA = "zariadenia"
    TN_ZASTUPCOVIA = "zariadenia_odborni_zastupcovia"
    TN_ZASTUPUJUCI_POSKYTOVATELIA = "zariadenia_zastupujuci_posktytovatelia"

    # Since we're wrangling LekarneHsrvester to partially handle also
    # 'zariadenie', we need to tweak naming of the key used to map other
    # records back to 'zariadenie'. Similarly for some other items.
    KONTAKT_BACKREFERENCE_KEY = "zariadenie_id"
    ZASTUPCA_BACKREFERENCE_KEY = "zariadenie_id"
    # for some reason, PC IDs in 'zariadenia' are empty => we'll assign ours
    USE_PC_ID_FROM_DATASET = False

    def __init__(self, cfg_parser, common_resources):
        super().__init__(cfg_parser, common_resources)
        self.poskytovatel_dict = None
        self.zariadenie_dict = None
        self.poistovna_dict = self.poistovna_id_counter = None
        self.zastupcovia_list = None
        self.odbory_list = self.odbory_ups_list = self.odbory_id_counter = None
        self.nepritomnost_list = self.nepritomnost_id_counter = None
        self.zp_list = None

        # wider hint: "rozpis sluzieb" is in essence very similar to "prevadzkova casy",
        # hence existing code was copied and following "renaming" done:
        # a) 'pohotovost' -> 'rozpis'
        # b) 'sluzba' remains
        # And then code was tweaked to adjust for different items and naming.
        self.rozpis_list = None
        self.sluzba_list = None

    def parse_sluzba(self, xml_element, rozpis_id):
        sluzba = {}

        self.parse_elements(
            xml_element,
            sluzba,
            ["DATUM_SLUZBY", "FORMA_SLUZBY", "SLUZBA_OD", "SLUZBA_DO", "ZVYRAZNIT"],
        )
        self.parse_elements(
            xml_element, sluzba, ["POZNAMKA", "SESTRA"], mandatory=False
        )
        sluzba["rozpis_id"] = rozpis_id
        # outlier: 'poskytovatel': We'd take just ID and skip the rest (sine the rest should be same as
        # in 'poskytovatel' table) BUT for few records ID is missing and say only 'CorporateBodyFullName'
        # is available. Hence we're taking all.
        xml_poskytovatel = xml_element.find("POSKYTOVATEL")
        self.parse_elements(
            xml_poskytovatel,
            sluzba,
            ["CorporateBodyFullName", "ICO", "ID"],
            mandatory=False,
        )

        # type conversions
        self.apply_conversion(sluzba, ["datum_sluzby"], self.convert_date)
        self.apply_conversion(sluzba, ["sluzba_od"], self.convert_time)
        self.apply_conversion(sluzba, ["sluzba_do"], self.convert_time_with_24handling)
        self.apply_conversion(sluzba, ["zvyraznit"], self.convert_bool)
        # outlier: sometimes not present (!!!) + rename key/column for brevity
        # (since it is coming from 'poskytovatel.id')
        if "id" in sluzba:
            sluzba["poskytovatel_id"] = self.convert_int(sluzba.pop("id"))
        else:
            logger.debug(
                "missing ID of 'poskytovatel' for 'sluzba' \"%s\", 'rozpis' %d (note: foreign key will not work)"
                % (sluzba["datum_sluzby"], rozpis_id)
            )
            sluzba["poskytovatel_id"] = -1

        return sluzba

    def parse_rozpis(self, xml_element):
        rozpis = {}

        self.parse_elements(xml_element, rozpis, ["INFO", "MESIAC", "PDF_FILE", "ROK"])
        # some stuff from nested element
        xml_rozpis = xml_element.find("ROZPIS")
        # note 1: 'mandant_id' is same as 'mandant' in URL, i.e. always same = > skipped on purpose
        # note 2: skipping 'poskytovatel' and 'zariadenie' items except IDs, since we have those
        #         (presumably same) values in separate tables linked via ID
        # note 3: skipping info which is already in parent element (e.h. 'rok, etc.)
        self.parse_elements(
            xml_rozpis,
            rozpis,
            [
                "ROZPIS_DATUM_SCHVALENIA",
                "ROZPIS_ID",
                "POSKYTOVATEL_ID",
                "ZARIADENIE_ID",
            ],
        )

        # type conversions
        self.apply_conversion(
            rozpis,
            ["mesiac", "poskytovatel_id", "rok", "rozpis_id", "zariadenie_id"],
            self.convert_int,
        )
        self.apply_conversion(rozpis, ["rozpis_datum_schvalenia"], self.convert_date)

        # duty status (sluzba)
        for xml_sluzba in xml_element.findall("SLUZBA"):
            sluzba = self.parse_sluzba(xml_sluzba, rozpis["rozpis_id"])
            self.sluzba_list.append(sluzba)

        return rozpis

    def parse_rozpis_sluzieb(self, xml_element, zariadenie):
        xml_rozpis_sluzieb = xml_element.find("ROZPIS_SLUZIEB")
        if xml_rozpis_sluzieb:
            rozpis = self.parse_rozpis(xml_rozpis_sluzieb)
            self.rozpis_list.append(rozpis)

    def parse_odbor(self, xml_element, zariadenie_id):
        odbor = {}
        odbor["odbor_id"] = xml_element.text
        odbor["zariadenie_id"] = zariadenie_id
        # no ID -> invent/assign some
        # (note: Same pool of IDs is reused for 'odbory_list' and 'odbory_ups' list, but
        # IDs are still unique, hence no big deal and two same/similar methods avoided.)
        odbor["id"] = self.odbory_id_counter
        self.odbory_id_counter += 1

        return odbor

    def parse_odbory(self, target_list, xml_element, zariadenie_id):
        for xml_odbor in xml_element.findall("ODBOR_ID"):
            odbor = self.parse_odbor(xml_odbor, zariadenie_id)
            target_list.append(odbor)

    def parse_poistovna(self, xml_element):
        if xml_element.text != "TRUE":
            return None  # not observed, but make sure we handle properly the likes of '<ACME_INSURANCE>FALSE</ACME_INSURANCE>

        meno = xml_element.tag
        if meno in self.poistovna_dict:
            return self.poistovna_dict[meno]

        poistovna = {}
        poistovna["meno"] = xml_element.tag
        # no ID -> invent/assign some
        poistovna["id"] = self.poistovna_id_counter
        self.poistovna_id_counter += 1

        self.poistovna_dict[meno] = poistovna

        return poistovna

    def parse_poistovne(self, xml_element):
        poistovna_id_list = []

        xml_poistovne = xml_element.find("POISTOVNE")
        if xml_poistovne:
            for xml_poistovna in xml_poistovne:
                poistovna = self.parse_poistovna(xml_poistovna)
                if poistovna:
                    poistovna_id_list.append(poistovna["id"])

        return poistovna_id_list

    def parse_zp(self, xml_element, nepritomnost_id):
        zp = {}

        self.parse_elements(xml_element, zp, ["POZNAMKA"])
        zp["nepritomnost_id"] = nepritomnost_id
        # optional stuff:
        self.parse_elements(
            xml_element,
            zp,
            [
                "MIESTO_PREVADZKY",
                "ODBORNY_ZASTUPCA",
                "POSKYTOVATEL",
                "POZNAMKA",
                "ZARIADENIE_ID",
            ],
            mandatory=False,
        )

        # type conversions
        self.apply_conversion(zp, ["zariadenie_id"], self.convert_int, mandatory=False)

        return zp

    def parse_zp_list(self, xml_element, nepritomnost_id):
        # stand-ins (zastupujuci poskytovatelia), optional
        xml_zp_list = xml_element.find("ZASTUPUJUCI_POSKYTOVATELIA")
        if xml_zp_list:
            for xml_kontakt in xml_zp_list.findall("ZASTUPUJUCI_POSKYTOVATEL"):
                zp = self.parse_zp(xml_kontakt, nepritomnost_id)
                self.zp_list.append(zp)

    def parse_nepritomnost(self, xml_element, zariadenie_id):
        nepritomnost = {}

        self.parse_elements(
            xml_element, nepritomnost, ["OD", "DO", "INFO", "ZASTUP_V_AMBULANCII"]
        )
        nepritomnost[self.KONTAKT_BACKREFERENCE_KEY] = zariadenie_id
        # no ID -> invent/assign some
        nepritomnost["id"] = self.nepritomnost_id_counter
        self.nepritomnost_id_counter += 1

        # type conversions
        self.apply_conversion(nepritomnost, ["od", "do"], self.convert_date)
        self.apply_conversion(nepritomnost, ["zastup_v_ambulancii"], self.convert_bool)

        # nested stuff:
        self.parse_zp_list(xml_element, nepritomnost["id"])

        return nepritomnost

    def parse_nepritomnosti(self, xml_element, zariadenie_id):
        # absences (nepritomnosti), optional
        xml_nepritomnosti = xml_element.find("NEPRITOMNOSTI")
        if xml_nepritomnosti:
            for xml_kontakt in xml_nepritomnosti.findall("NEPRITOMNOST"):
                nepritomnost = self.parse_nepritomnost(xml_kontakt, zariadenie_id)
                self.nepritomnost_list.append(nepritomnost)

    def parse_zariadenie(self, xml_element):
        """
        parse XML record of one pharmacy

        roughly equivalent (a.k.a. very similar) to `LekarneHargvester.parse_lekaren()`
        """
        zariadenie = {}
        self.parse_elements(
            xml_element,
            zariadenie,
            [
                "DRUH_ZARIADENIA",
                "ID",
                "NAZOV_ZARIADENIA",
                "POZNAMKA_NA_WEB",
                "PRIAMA_PLATBA",
                "TYP_ZARIADENIA_ID",
                "TYP_ZARIADENIA_COMPLEX_CODE",
                "UPOZORNENIE",
                "VYHLADAVANIE",
            ],
        )
        self.parse_elements(
            xml_element,
            zariadenie,
            [
                "IDENTIFIKATOR",
                "POLOHA_LAT",
                "POLOHA_LON",
                "VYPOCITANA_POLOHA",
            ],
            mandatory=False,
        )
        self.parse_physical_address(xml_element, zariadenie)
        # sort of outliers
        zariadenie["druh_zariadenia_kod"] = xml_element.find("DRUH_ZARIADENIA").attrib[
            "KOD"
        ]
        # - 'PODTYPY_ZARIADENI' is present always, 'PODTYP_ZARIADENIA' was observed occurring 0 or 1 times
        #   hence assumption (to simplify DB model): we have n more than 1 'PODTYP_ZARIADENIA'
        xml_podtyp = xml_element.find("PODTYPY_ZARIADENI").find("PODTYP_ZARIADENIA")
        if xml_podtyp:
            zariadenie["podtyp_zariadenia"] = xml_podtyp.text
        else:
            zariadenie["podtyp_zariadenia"] = None
        # - well, sometimes KPZS is missing
        xml_kpzs_kod = xml_element.find("KPZS")
        if xml_kpzs_kod:
            zariadenie["kpzs_kod"] = xml_kpzs_kod.find("KOD").text
        else:
            logger.debug("no KPZS available for 'zariadenie': %s" % zariadenie["id"])
            zariadenie["kpzs_kod"] = None

        # type conversions
        zariadenie_id = self.convert_int(zariadenie["id"])
        zariadenie["id"] = zariadenie_id
        self.apply_conversion(zariadenie, ["typ_zariadenia_id"], self.convert_int)
        self.apply_conversion(
            zariadenie, ["vypocitana_poloha"], self.convert_bool, mandatory=False
        )
        self.apply_conversion(
            zariadenie,
            ["poloha_lat", "poloha_lon"],
            self.convert_lat_lon,
            mandatory=False,
        )

        # insurance organizations:
        zariadenie["poistovne"] = self.parse_poistovne(xml_element)

        xml_odbory = xml_element.find("ODBORY")
        if xml_odbory:
            self.parse_odbory(self.odbory_list, xml_odbory, zariadenie_id)
        xml_odbory_ups = xml_element.find("ODBORY_UPS")
        if xml_odbory_ups:
            self.parse_odbory(self.odbory_ups_list, xml_odbory_ups, zariadenie_id)

        # other nested stuff:
        self.parse_kontakty(xml_element, zariadenie_id)
        self.parse_prevadzkove_casy(xml_element, zariadenie_id)
        self.parse_zastupcovia(
            xml_element, "ODBORNI_ZASTUPCOVIA", "ODBORNY_ZASTUPCA", zariadenie_id
        )
        self.parse_rozpis_sluzieb(xml_element, zariadenie)
        self.parse_physical_address(xml_element, zariadenie)
        self.parse_nepritomnosti(xml_element, zariadenie_id)

        return zariadenie

    def parse_zariadenia(self, xml_element, poskytovatel):
        """
        parse list of facilities in given XML element
        """

        zariadenia = {}
        for xml_zariadenie in xml_element.findall("ZARIADENIE"):
            zariadenie = self.parse_zariadenie(xml_zariadenie)
            zariadenie["poskytovatel_id"] = poskytovatel["id"]

            if zariadenie["id"] in self.zariadenie_dict:
                logger.warn(
                    "duplicate ID of 'zariadenie' detected: %d (name: \"%s\")- dropping the entry"
                    % (zariadenie["id"], zariadenie["nazov_zariadenia"])
                )
            else:
                zariadenia[zariadenie["id"]] = zariadenie

        return zariadenia

    def parse_poskytovatel(self, xml_element):
        poskytovatel = {}
        self.parse_elements(
            xml_element, poskytovatel, ["ID", "CorporateBodyFullName", "ICO", "MZSR"]
        )
        logger.debug("processing 'poskytovatel': %s" % poskytovatel["id"])

        # type conversions
        self.apply_conversion(poskytovatel, ["id"], self.convert_int)
        self.apply_conversion(poskytovatel, ["mzsr"], self.convert_bool)

        # well, sometimes address is missing
        try:
            self.parse_physical_address(xml_element, poskytovatel)
        except KeyError:
            logger.debug(
                "no physical address available for 'poskytovatel': %s"
                % poskytovatel["id"]
            )

        return poskytovatel

    def parse_row(self, xml_row):
        poskytovatel = self.parse_poskytovatel(xml_row.find("POSKYTOVATEL"))
        zariadenie_dict = self.parse_zariadenia(
            xml_row.find("ZARIADENIA"), poskytovatel
        )
        # 'zariadenie_dict' is "for this particular poskytovatel"

        if poskytovatel["id"] in self.poskytovatel_dict:
            logger.warn(
                "duplicate ID of 'poskytovatel' detected: %d (ICO: %s)- dropping the entry"
                % (poskytovatel["id"], poskytovatel["ico"])
            )
        else:
            self.poskytovatel_dict[poskytovatel["id"]] = poskytovatel

        self.zariadenie_dict.update(zariadenie_dict)

    def process_dataset(self, xml_data):
        # we will be working with those dicts throughout
        # this "parsing session" (= one run of `run()`)
        self.poskytovatel_dict = {}
        self.zariadenie_dict = {}
        self.poistovna_dict = {}
        self.zastupcovia_list = []
        self.odbory_list = []
        self.odbory_ups_list = []
        self.rozpis_list = []
        self.sluzba_list = []
        self.nepritomnost_list = []
        self.zp_list = []
        self.poistovna_id_counter = 0
        self.odbory_id_counter = 0
        self.nepritomnost_id_counter = 0

        super().process_dataset(xml_data)

    def store_dataset(self):
        # convert lists to data frames and store them in DB
        df_poskytovatel = DataFrame(self.poskytovatel_dict.values())
        df_poskytovatel.set_index(
            "id", append=False, inplace=True, verify_integrity=True
        )
        df_poskytovatel.set_index(
            "ico", append=True, inplace=True, verify_integrity=True
        )
        self.store_table(df_poskytovatel, self.TN_POSKYTOVATEL)
        self.poskytovatel_dict = df_poskytovatel = None

        df_zariadenie = DataFrame(self.zariadenie_dict.values())
        df_zariadenie.set_index("id", append=False, inplace=True, verify_integrity=True)
        df_zariadenie.set_index(
            ["identifikator", "kpzs_kod"],
            append=True,
            inplace=True,
            verify_integrity=True,
        )
        self.store_table(df_zariadenie, self.TN_ZARIADENIA)
        self.zariadenie_dict = df_zariadenie = None

        df_poistovna = DataFrame(self.poistovna_dict.values())
        df_poistovna.set_index("id", append=False, inplace=True, verify_integrity=True)
        self.store_table(df_poistovna, self.TN_POISTOVNE)
        self.poistovna_dict = df_poistovna = None

        df_odbory = DataFrame(self.odbory_list)
        df_odbory.set_index("id", append=False, inplace=True, verify_integrity=True)
        df_odbory.set_index(
            "zariadenie_id", append=True, inplace=True, verify_integrity=True
        )
        self.store_table(df_odbory, self.TN_ODBORY)
        self.odbory_list = df_odbory = None

        df_odbory_ups = DataFrame(self.odbory_ups_list)
        df_odbory_ups.set_index("id", append=False, inplace=True, verify_integrity=True)
        df_odbory_ups.set_index(
            "zariadenie_id", append=True, inplace=True, verify_integrity=True
        )
        self.store_table(df_odbory_ups, self.TN_ODBORY_UPS)
        self.odbory_ups_list = df_odbory_ups = None

        self.store_df_kontakty(self.TN_KONTAKTY)

        self.store_df_pc(self.TN_PC, self.TN_PC_DNI, self.TN_PC_USEK_DNA)

        df_rozpis = DataFrame(self.rozpis_list)
        df_rozpis.set_index(
            "rozpis_id", append=False, inplace=True, verify_integrity=True
        )
        df_rozpis.set_index(
            ["rok", "mesiac", "poskytovatel_id", "zariadenie_id"],
            append=True,
            inplace=True,
            verify_integrity=True,
        )
        self.store_table(df_rozpis, self.TN_ROZPIS)
        self.rozpis_list = df_rozpis = None

        df_sluzba = DataFrame(self.sluzba_list)
        df_sluzba.rename_axis("id", inplace=True)
        df_sluzba.set_index(
            ["datum_sluzby", "poskytovatel_id", "rozpis_id"],
            append=True,
            inplace=True,
            verify_integrity=True,
        )
        # Some fields are optional and sometimes "all empty", then data type
        # will not be guessed properly, hence 'text' will be tried which will
        # cause problems like for example issue #11. So we help a little:
        df_sluzba_dtype = {
            "sluzba_od": sqlalchemy.Time(),
            "sluzba_od": sqlalchemy.Time(),
        }
        self.store_table(df_sluzba, self.TN_SLUZBA, dtype=df_sluzba_dtype)
        self.sluzba_list = df_sluzba = None

        df_nepritomnost = DataFrame(self.nepritomnost_list)
        df_nepritomnost.set_index(
            "id", append=False, inplace=True, verify_integrity=True
        )
        df_nepritomnost.set_index(
            self.KONTAKT_BACKREFERENCE_KEY,
            append=True,
            inplace=True,
            verify_integrity=True,
        )
        self.store_table(df_nepritomnost, self.TN_NEPRITOMNOSTI)
        self.nepritomnost_list = df_nepritomnost = None

        df_zp_list = DataFrame(self.zp_list)
        df_zp_list.sort_index(axis=1, inplace=True)
        df_zp_list.rename_axis("id", inplace=True)
        df_zp_list.set_index(
            "nepritomnost_id", append=True, inplace=True, verify_integrity=True
        )
        self.store_table(df_zp_list, self.TN_ZASTUPUJUCI_POSKYTOVATELIA)
        self.zp_list = df_zp_list = None

        # following are in extra methods, since re-used is 'ZariadeniaHarvester'
        self.store_df_zastupcovia(self.TN_ZASTUPCOVIA)
