#!/usr/bin/python3
#
# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl 5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# This harvests and keeps up-to-date statistical data from eVuc.sk
# in local PostgreeSQL database.
#
# Main points:
# a) on first run creates tables in empty DB and gets all data from eVuc.sk
# b) on subsequent runs deletes what is in DB and gets again all from eVuc.sk
#    (i.e. data removed at eVuc.sk are deleted, new data is fetched but also all
#    unchanged data is fetched repeatedly)
# c) one URL = one dataset = several DB tables
# d) given usage of Pandas, foreign keys are added "ex-post" using SQLAlchemy,
#    but only if the source data permits (i.e. all IDs are properly filled)

import configparser
import logging
import os
import sys
import time
import traceback
from argparse import ArgumentParser
from logging.config import fileConfig

from sqlalchemy.engine.reflection import Inspector
from sqlalchemy.exc import IntegrityError
from sqlalchemy.schema import AddConstraint, ForeignKeyConstraint, PrimaryKeyConstraint

from . import *
from .common import CommonHarvesterResources
from .common import common_config

from prometheus_client import CollectorRegistry, Gauge, Summary, push_to_gateway


fileConfig('logging_config.ini')
logger = logging.getLogger()


# Prometheus metrics
prom_registry = CollectorRegistry()

prom_harvest_time = Summary('request_processing_seconds', 'Time spent processing request', registry=prom_registry)
prom_last_harvest_time = Gauge('harvester_last_run', 'Last time the harvest was run (finished) in ms', registry=prom_registry)
prom_scanned_cubes = Gauge('harvester_scanned_datasets', 'Number of datasets scanned during the harvest', registry=prom_registry)
prom_updated_cubes = Gauge('harvester_updated_datasets', 'Number of datasets updated during the harvest', registry=prom_registry)
prom_errored_cubes = Gauge('harvester_failed_updating_datasets', 'Number of datasets that couldn\'t be updated due to a failure during the harvest', registry=prom_registry)


class EVucSkHarvester:

    RUN_REPORT = "harvesting complete; success: %d, errors: %d"

    HARVESTERS = {
        'lekarne': LekarneHarvester,
        'pohotovost': PohotovostHarvester,
        'socialne_sluzby': SocialneSluzbyHarvester,
        'zariadenia': ZariadeniaHarvester
    }


    def __init__(self):
        self.config = common_config
        cfg_parser = self.config.cfg_parser

        # logging setup for requests library
        requests_log = logging.getLogger("urllib3")
        requests_log.propagate = True

        # command-line options
        harvester_names = self._parse_cmdln_options()
        if len(harvester_names) <= 0:
            harvester_names = self.HARVESTERS.keys()

        # initialize desired harvesters
        self.harvesters = []
        self.common_resources = CommonHarvesterResources(self.config)
        for harvester_name in harvester_names:
            try:
                self.harvesters.append(self.HARVESTERS[harvester_name](cfg_parser, self.common_resources))
            except RuntimeError as e:
                logger.error(e)
                logger.error(traceback.format_exc())


    def _parse_cmdln_options(self):
        parser = ArgumentParser()
        parser.add_argument(
            'harvesters', metavar='harvester', nargs='*',
            help='a harvester to run')
        parser.add_argument(
            '-v', '--verbose',
            action="store_true", dest="verbose", default=False,
            help="be more verbose")
        args = parser.parse_args()

        if args.verbose:
            logging.getLogger().setLevel('DEBUG')

        return args.harvesters


    def fk_exitsts(self, table_name, table_column_name):
        fk_list = self.common_resources.inspect.get_foreign_keys(table_name, schema=self.config.db_schema)
        if len(self.common_resources.inspect.get_foreign_keys(table_name, schema=self.config.db_schema)) <= 0:
            return False

        for fk in fk_list:
            if table_column_name in fk['constrained_columns']:
                return True

        return False


    def add_foreign_key(self, table_name, table_column_name, foreign_table_name, foreign_column_name):
        # we were maybe creating tables => let's refresh
        self.common_resources.metadata.reflect(self.common_resources.engine, schema=self.common_resources.config.db_schema)

        full_table_name = self.config.db_schema + '.' + foreign_table_name
        if full_table_name not in self.common_resources.metadata.tables:
            logger.debug('table %s not present (harvester disabled?), skipping' % full_table_name)
            return

        # first: add primary key constraint
        foreign_table = self.common_resources.metadata.tables[full_table_name]
        if len(foreign_table.primary_key) <= 0:
            constraint = PrimaryKeyConstraint(foreign_column_name)
            foreign_table.append_constraint(constraint)
            add_constraint = AddConstraint(constraint)
            self.common_resources.session.execute(add_constraint)
            logger.info('added primary key: %s.%s' %
                         (foreign_table_name, foreign_column_name))

        # only then: add foreign key
        try:
            if not self.fk_exitsts(table_name, table_column_name):
                table = self.common_resources.metadata.tables[self.config.db_schema + '.' + table_name]
                constraint = ForeignKeyConstraint(
                    [table_column_name],
                    ['%s.%s' % (foreign_table_name, foreign_column_name)],
                    ondelete="SET NULL")
                table.append_constraint(constraint)
                add_constraint = AddConstraint(constraint)
                self.common_resources.session.execute(add_constraint)

                self.common_resources.session.commit()
                logger.info('added foreign key: %s.%s -> %s.%s' %
                             (table_name, table_column_name, foreign_table_name, foreign_column_name))
        except IntegrityError as e:
            logger.warning('foreign key %s.%s -> %s.%s creation failed: %s' %
                         (table_name, table_column_name, foreign_table_name, foreign_column_name, e))
            self.common_resources.session.rollback()


    def add_foreign_keys(self):
        # This is a hack which attempts to ease the life for users of harvested
        # date by adding foreign key mappings. Given that Pandas is used to
        # created tables and given that Pandas for now does not provide feature(s)
        # allowing foreign keys between several DataFrames (quite obviously, it does
        # not make sense for DataFrames, since foreign keys are "SQL stuff"),
        # we do this here in the form of "patching"/workaround.

        # lekarne:
        self.add_foreign_key(
            LekarneHarvester.TN_KONTAKTY, LekarneHarvester.KONTAKT_BACKREFERENCE_KEY,
            LekarneHarvester.TN_LEKARNE, 'lekaren_id')
        self.add_foreign_key(
            LekarneHarvester.TN_LEKARNE, 'prevadzkovatel_id',
            LekarneHarvester.TN_PREVADZKOVATEL, 'id')
        self.add_foreign_key(
            LekarneHarvester.TN_PC, 'lekaren_id',
            LekarneHarvester.TN_LEKARNE, 'lekaren_id')
        self.add_foreign_key(
            LekarneHarvester.TN_PC_DNI, 'pc_id',
            LekarneHarvester.TN_PC, 'id')
        self.add_foreign_key(
            LekarneHarvester.TN_PC_USEK_DNA, 'den_id',
            LekarneHarvester.TN_PC_DNI, 'id')
        self.add_foreign_key(
            LekarneHarvester.TN_PREKAZKA, 'lekaren_id',
            LekarneHarvester.TN_LEKARNE, 'lekaren_id')

        # pohotovost:
        self.add_foreign_key(
            PohotovostHarvester.TN_SLUZBA, 'pohotovost_id',
            PohotovostHarvester.TN_POHOTOVOST, 'id')
        # This index is disabled since since it sometimes occurring that a non-existent 'lekaren_id'
        # is used in 'pohotovost_sluzba'.
        #self.add_foreign_key(
        #    PohotovostHarvester.TN_SLUZBA, 'lekaren_id',
        #    LekarneHarvester.TN_LEKARNE, 'lekaren_id')

        # zariadenia:
        self.add_foreign_key(
            ZariadeniaHarvester.TN_ZARIADENIA, 'poskytovatel_id',
            ZariadeniaHarvester.TN_POSKYTOVATEL, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_KONTAKTY, ZariadeniaHarvester.KONTAKT_BACKREFERENCE_KEY,
            ZariadeniaHarvester.TN_ZARIADENIA, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_ZASTUPCOVIA, 'zariadenie_id',
            ZariadeniaHarvester.TN_ZARIADENIA, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_ODBORY, 'zariadenie_id',
            ZariadeniaHarvester.TN_ZARIADENIA, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_ODBORY_UPS, 'zariadenie_id',
            ZariadeniaHarvester.TN_ZARIADENIA, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_PC, 'zariadenie_id',
            ZariadeniaHarvester.TN_ZARIADENIA, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_PC_DNI, 'pc_id',
            ZariadeniaHarvester.TN_PC, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_PC_USEK_DNA, 'den_id',
            ZariadeniaHarvester.TN_PC_DNI, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_ROZPIS, 'poskytovatel_id',
            ZariadeniaHarvester.TN_POSKYTOVATEL, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_ROZPIS, 'zariadenie_id',
            ZariadeniaHarvester.TN_ZARIADENIA, 'id')
        # This index is disabled since it is permitted (and usually does occur in few items)
        # that 'poskytovatel_id' is empty, because eVuc.sk allows users to fill also "free style"
        # naming of 'CorporateBodyFullName', i.e. using names which are not in main list of
        # entities ('poskytovatel').
        #self.add_foreign_key(
        #    ZariadeniaHarvester.TN_SLUZBA, 'poskytovatel_id',
        #    ZariadeniaHarvester.TN_POSKYTOVATEL, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_SLUZBA, 'rozpis_id',
            ZariadeniaHarvester.TN_ROZPIS, 'rozpis_id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_NEPRITOMNOSTI, 'zariadenie_id',
            ZariadeniaHarvester.TN_ZARIADENIA, 'id')
        self.add_foreign_key(
            ZariadeniaHarvester.TN_ZASTUPUJUCI_POSKYTOVATELIA, 'nepritomnost_id',
            ZariadeniaHarvester.TN_NEPRITOMNOSTI, 'id')

        # socialne_sluzby:
        self.add_foreign_key(
            SocialneSluzbyHarvester.TN_SOC_SLUZBY, 'poskytovatel_id',
            SocialneSluzbyHarvester.TN_POSKYTOVATELIA, 'id')
        self.add_foreign_key(
            SocialneSluzbyHarvester.TN_INET_ADDR, 'sluzba_id',
            SocialneSluzbyHarvester.TN_SOC_SLUZBY, 'id')
        self.add_foreign_key(
            SocialneSluzbyHarvester.TN_KONTAKTY, SocialneSluzbyHarvester.KONTAKT_BACKREFERENCE_KEY,
            SocialneSluzbyHarvester.TN_SOC_SLUZBY, 'id')
        self.add_foreign_key(
            SocialneSluzbyHarvester.TN_ZASTUPCOVIA, 'sluzba_id',
            SocialneSluzbyHarvester.TN_SOC_SLUZBY, 'id')


    @prom_harvest_time.time()
    def main(self):
        counter_success = 0
        counter_fail = 0

        # run harvesters (note: we may later run them in parallel?)
        for harvester in self.harvesters:
            try:
                harvester.run()
                counter_success += 1
            except Exception as e:
                counter_fail += 1
                logger.error(e)
                logger.error(traceback.format_exc())

        self.add_foreign_keys()

        result_success = not (counter_success == 0 and counter_fail > 0)
        if result_success:
            logger.info(self.RUN_REPORT, counter_success, counter_fail)
        else:
            logger.error(self.RUN_REPORT, counter_success, counter_fail)

        # Send metrics to prometheus pushgateway
        if self.common_resources.config.prometheus_uri:
            try:
                prom_scanned_cubes.set(len(self.harvesters))
                prom_updated_cubes.set(counter_success)
                prom_errored_cubes.set(counter_fail)
                prom_last_harvest_time.set(time.time() * 1000)
                logger.info('Pushing metrics to prometheus')
                push_to_gateway(self.common_resources.config.prometheus_uri, job='evucsk-harvester', registry=prom_registry)
            except Exception:
                logger.error("Unable to reach prometheus pushgateway {}".format(self.common_resources.config.prometheus_uri))

        return result_success


if __name__ == '__main__':
    evucsk_harvester = EVucSkHarvester()
    success = evucsk_harvester.main()
    if not success:
        sys.exit(-1)
