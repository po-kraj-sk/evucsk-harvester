# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl 5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# General notes about how the XML data is processed:
#
# 1) There is 1-N relationship between 'sluzba' and 'lekaren' via 'lekaren_id'.
# As with all relationships in harvested data, no explicit "foreign key" is
# maintained thus relation can be "inferred" with
# "sluzba.lekaren.id -> lekaren.lekaren_id".
#
# 2) Data in "pohotost" dataset is deconstructed into multiple tables. To link
# the data in DB together, 'id' is "manufactured" for main table and used
# primary key.
#
#
# Terminology:
# - cas: time
# - den: day
# - lekaren: pharmacy
# - mesiac: month
# - okres: county
# - pohotovost: emergency service
# - rok: year
# - sluzba: duty status

from xml.etree import ElementTree

from pandas import DataFrame

from .common import HarvesterBase


class PohotovostHarvester(HarvesterBase):

    CONFIG_SECTION = "pohotovost"

    TN_POHOTOVOST = "pohotovost"
    TN_SLUZBA = "pohotovost_sluzba"

    def __init__(self, cfg_parser, common_resources):
        super().__init__(cfg_parser, common_resources)
        self.pohotovost_list = self.pohotovost_id_counter = None
        self.sluzba_list = None

    def parse_sluzba(self, xml_element, pohotovost_id):
        sluzba = {}

        self.parse_elements(
            xml_element, sluzba, ["CAS_OD", "CAS_DO", "DEN", "LEKAREN_ID", "ZVYRAZNIT"]
        )
        sluzba["pohotovost_id"] = pohotovost_id

        # type conversions
        self.apply_conversion(sluzba, ["cas_od"], self.convert_time)
        self.apply_conversion(sluzba, ["cas_do"], self.convert_time_with_24handling)
        self.apply_conversion(sluzba, ["den", "lekaren_id"], self.convert_int)
        self.apply_conversion(sluzba, ["zvyraznit"], self.convert_bool)

        return sluzba

    def parse_pohotovost(self, xml_element):
        pohotovost = {}
        # note: 'mandant_id' is same as 'mandant' in URL, i.e. always same = > skipped on purpose
        self.parse_elements(
            xml_element,
            pohotovost,
            [
                "MESIAC",
                "OKRES",
                "OKRES_CODE",
                "PDF_FILE",
                "PUBLIKOVANIE_DATUM",
                "ROK",
                "TYP",
            ],
        )
        # no ID -> invent/assign some
        pohotovost["id"] = self.pohotovost_id_counter
        self.pohotovost_id_counter += 1

        # type conversions
        self.apply_conversion(pohotovost, ["mesiac", "rok"], self.convert_int)
        self.apply_conversion(pohotovost, ["publikovanie_datum"], self.convert_date)

        # duty status (sluzba)
        for xml_sluzba in xml_element.findall("SLUZBA"):
            sluzba = self.parse_sluzba(xml_sluzba, pohotovost["id"])
            self.sluzba_list.append(sluzba)

        return pohotovost

    def parse_row(self, xml_row):
        pohotovost = self.parse_pohotovost(xml_row)
        self.pohotovost_list.append(pohotovost)

    def process_dataset(self, xml_data):
        # we will be working with those dicts throughout
        # this "parsing session" (= one run of `run()`)
        self.pohotovost_list = []
        self.sluzba_list = []
        self.pohotovost_id_counter = 0

        xml_root = ElementTree.fromstring(xml_data)
        for row in xml_root.findall("POHOTOVOST_ROZPIS"):
            self.parse_row(row)

    def store_dataset(self):
        # convert lists to data frames and store them in DB
        df_pohotovost = DataFrame(self.pohotovost_list)
        df_pohotovost.set_index("id", append=False, inplace=True, verify_integrity=True)
        df_pohotovost.set_index(
            ["rok", "mesiac", "okres_code", "typ"],
            append=True,
            inplace=True,
            verify_integrity=True,
        )
        self.store_table(df_pohotovost, self.TN_POHOTOVOST)
        self.pohotovost_list = df_pohotovost = None

        df_sluzba = DataFrame(self.sluzba_list)
        df_sluzba.rename_axis("id", inplace=True)
        df_sluzba.set_index(
            ["lekaren_id", "den"], append=True, inplace=True, verify_integrity=True
        )
        self.store_table(df_sluzba, self.TN_SLUZBA)
        self.sluzba_list = df_sluzba = None
