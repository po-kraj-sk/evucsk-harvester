# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl 5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# General notes about how the XML data is processed:
#
# 1) There is 1-N relationship between 'sluzba' and 'lekaren' via 'lekaren_id'.
# As with all relationships in harvested data, no explicit "foreign key" is
# maintained thus relation can be "inferred" with
# "sluzba.lekaren.id -> lekaren.lekaren_id".
#
# 2) Data in "pohotost" dataset is deconstructed into multiple tables. To link
# the data in DB together, 'id' is "manufactured" for main table and used
# primary key.
#
#
# Terminology:
# - poskytovatel: entity providing one or more services
# - socialne sluzby: social/societal services

import logging
from xml.etree import ElementTree

from pandas import DataFrame
import sqlalchemy

from .common import HarvesterBase


class SocialneSluzbyHarvester(HarvesterBase):

    CONFIG_SECTION = 'socialne_sluzby'

    TN_POSKYTOVATELIA = 'soc_sluzby_poskytovatelia'
    TN_SOC_SLUZBY = 'soc_sluzby'
    TN_INET_ADDR = 'soc_sluzby_inet_addr'
    TN_KONTAKTY = 'soc_sluzby_kontakty'
    TN_ZASTUPCOVIA = 'soc_sluzby_zodpovedni_zastupcovia'

    # Since we're wrangling LekarneHsrvester to partially handle also
    # 'zariadenie', we need to tweak naming of the key used to map other
    # records back to 'zariadenie'. Similarly for some other items. 
    KONTAKT_BACKREFERENCE_KEY = 'sluzba_id'
    ZASTUPCA_BACKREFERENCE_KEY = 'sluzba_id'


    def __init__(self, cfg_parser, common_resources):
        super().__init__(cfg_parser, common_resources)
        self.poskytovatel_list = None
        self.sluzba_list = None
        self.kontakt_list = None
        self.zastupcovia_list = None    # re-used from 'zariadenia', here used for 'zodpovedni_zastupcovia'
        self.inet_addr_list = None


    def parse_inet_addr(self, xml_element):
        inet_addr = {}
        self.parse_elements(xml_element, inet_addr, ['Address'])
        return inet_addr


    def parse_inet_addresses(self, xml_element, sluzba_id):
        xml_inet_addresses = xml_element.find('InternetAddresses')
        if xml_inet_addresses:
            for xml_inet_addr in xml_inet_addresses.findall('InternetAddress'):
                inet_addr = self.parse_inet_addr(xml_inet_addr)
                inet_addr['sluzba_id'] = sluzba_id
                self.inet_addr_list.append(inet_addr)


    def parse_sluzba(self, xml_element, poskytovatel_id):
        sluzba = {}

        self.parse_elements(
            xml_element,
            sluzba,
            [
                'CIELOVA_SKUPINA',
                'ID',
                'DRUH_SOCIALNEJ_SLUZBY',
                'DRUH_SOCIALNEJ_SLUZBY_POZNAMKA',
                'FORMA_SOCIALNEJ_SLUZBY',
                'DEN_ZACATIA_POSKYTOVANIA',
                'DEN_UKONCENIA_POSKYTOVANIA',
                'DOPLNUJUCA_INFORMACIA',
                'INFO_NA_WEB',
                'KAPACITA',
                'KAPACITA_ZIEN',
                'ROZSAH_SOCIALNEJ_SLUZBY'
                ])
        self.parse_elements(
            xml_element,
            sluzba,
            [
                'POLOHA_LAT',
                'POLOHA_LON',
                'VYPOCITANA_POLOHA',
                'WEB'
                ],
            mandatory=False)
        self.parse_physical_address(xml_element, sluzba, mandatory=False)
        sluzba['poskytovatel_id'] = poskytovatel_id
        # sort of outliers:
        sluzba['druh_socialnej_sluzby_kod'] = xml_element.find('DRUH_SOCIALNEJ_SLUZBY').attrib['KOD']
        xml_uzemna_posobnost = xml_element.find('UZEMNA_POSOBNOST')
        if xml_uzemna_posobnost:
            self.parse_elements(xml_uzemna_posobnost, sluzba, ['OBEC', 'OKRES', 'KRAJ'])

        # type conversions
        self.apply_conversion(
            sluzba,
            ['den_zacatia_poskytovania', 'den_ukoncenia_poskytovania'],
            self.convert_date)
        self.apply_conversion(
            sluzba,
            ['id', 'druh_socialnej_sluzby_kod', 'kapacita', 'kapacita_zien'],
            self.convert_int)
        self.apply_conversion(sluzba, ['vypocitana_poloha'], self.convert_bool, mandatory=False)
        self.apply_conversion(sluzba, ['poloha_lat', 'poloha_lon'], self.convert_lat_lon, mandatory=False)

        # other nested stuff:
        sluzba_id = sluzba['id']
        self.parse_kontakty(xml_element, sluzba_id)
        self.parse_zastupcovia(
            xml_element,
            'ZODPOVEDNI_ZASTUPCOVIA',
            'ZODPOVEDNY_ZASTUPCA',
            sluzba_id)
        self.parse_inet_addresses(xml_element, sluzba_id)

        return sluzba


    def parse_poskytovatel(self, xml_element):
        poskytovatel = {}
        # note: 'mandant_id' is same as 'mandant' in URL, i.e. always same = > skipped on purpose
        self.parse_elements(
            xml_element,
            poskytovatel,
            [
                'CorporateBodyFullName',
                'DATUM_ZAPISU_DO_REGISTRA',
                'DATUM_ZMENY_V_REGISTRI',
                'ID',
                'ICO',
                'LegalForm',
                'REGISTRACNE_CISLO',
                'TYP_POSKYTOVATELA',
                'ZRIADOVATEL'
                ])
        self.parse_elements(
            xml_element,
            poskytovatel,
            ['SEMAFOR'],
            mandatory=False)
        logging.debug('processing poskytovatel: %s' % poskytovatel['id'])
        self.parse_physical_address(xml_element, poskytovatel)

        # type conversions
        self.apply_conversion(poskytovatel, ['id', 'registracne_cislo'], self.convert_int)
        self.apply_conversion(
            poskytovatel,
            ['datum_zapisu_do_registra', 'datum_zmeny_v_registri'],
            self.convert_date)

        # offered social services
        xml_services = xml_element.find('POSKYTOVANE_SOCIALNE_SLUZBY')
        if xml_services:
            for xml_sluzba in xml_services.findall('SOCIALNA_SLUZBA'):
                sluzba = self.parse_sluzba(xml_sluzba, poskytovatel['id'])
                self.sluzba_list.append(sluzba)

        return poskytovatel


    def parse_row(self, xml_row):
        poskytovatel = self.parse_poskytovatel(xml_row)
        self.poskytovatel_list.append(poskytovatel)


    def process_dataset(self, xml_data):
        # we will be working with those dicts throughout
        # this "parsing session" (= one run of `run()`)
        self.poskytovatel_list = []
        self.sluzba_list = []
        self.kontakt_list = []
        self.zastupcovia_list = []
        self.inet_addr_list = []

        xml_root = ElementTree.fromstring(xml_data)
        for row in xml_root.findall('POSKYTOVATEL'):
            self.parse_row(row)


    def store_dataset(self):
        # convert lists to data frames and store them in DB
        df_poskytovatel = DataFrame(self.poskytovatel_list)
        df_poskytovatel.set_index('id', append=False, inplace=True, verify_integrity=True)
        self.store_table(df_poskytovatel, self.TN_POSKYTOVATELIA)
        self.poskytovatel_list = df_poskytovatel = None

        df_sluzba = DataFrame(self.sluzba_list)
        df_sluzba.set_index('id', append=False, inplace=True, verify_integrity=True)
        df_sluzba.set_index(['poskytovatel_id'],
                            append=True, inplace=True, verify_integrity=True)
        # Some fields are optional and sometimes "all empty", then data type
        # will not be guessed properly, hence 'text' will be tried which will
        # cause problems like for example issue #11. So we help a little:
        df_sluzba_dtype = {
            'den_zacatia_poskytovania': sqlalchemy.Date(),
            'den_ukoncenia_poskytovania': sqlalchemy.Date()
            }
        self.store_table(df_sluzba, self.TN_SOC_SLUZBY, dtype=df_sluzba_dtype)
        self.sluzba_list = df_sluzba = None

        df_inet_addr = DataFrame(self.inet_addr_list)
        df_inet_addr.rename_axis('id', inplace=True)
        df_inet_addr.set_index(['sluzba_id'],
                               append=True, inplace=True, verify_integrity=True)
        self.store_table(df_inet_addr, self.TN_INET_ADDR)
        self.inet_addr_list = df_inet_addr = None

        self.store_df_kontakty(self.TN_KONTAKTY)
        self.store_df_zastupcovia(self.TN_ZASTUPCOVIA)
