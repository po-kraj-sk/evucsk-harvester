# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl 5
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# General notes about how the XML data is processed applicable to all datasets (e.g. XML files):
#
# 1) Since we do not have 'last modification time' and we also can not rely on
# either "new data at the end" or "only new data present", we're pushing all data
# into DB, removing anything which was there before, i.e. quite expensive if quite
# often very little or even nothing will change in source data.

import configparser
import locale
import logging
import os
import re
import sys
import tempfile
import time
from datetime import datetime

from git import Repo
from git.exc import GitCommandError

from pandas import DataFrame

import requests
import requests_cache
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

import sqlalchemy
from sqlalchemy import create_engine, inspect
from sqlalchemy import MetaData
from sqlalchemy.orm import declarative_base, sessionmaker


logger = logging.getLogger()


class CommonConfig:

    CONFIG_FILE_NAME = "evucsk_harvester.conf"
    CONFIG_SECTION_HARVESTER = "evucsk"
    CONFIG_SECTION_PROMETHEUS = "prometheus"
    CONFIG_SECTION_REQUESTS = "requests"
    CONFIG_SECTION_SQLALCHEMY = "sqlalchemy"

    def __init__(self):
        # monkey-patch -> ugly, but env. variables are case insensitive and thus with default
        # behavior of ConfigParser may trigger duplicates exception
        configparser.ConfigParser.optionxform = str
        self.cfg_parser = configparser.ConfigParser(os.environ)
        self.cfg_parser.read(self.CONFIG_FILE_NAME)

        try:
            # harvester - mandatory:
            self.db_string = self.cfg_parser.get(self.CONFIG_SECTION_HARVESTER, "db")
            self.evucsk_user = self.cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "user"
            )
            self.evucsk_password = self.cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "password"
            )

            # harvester - optional:
            self.be_careful = self.cfg_parser.getboolean(
                self.CONFIG_SECTION_HARVESTER, "be_careful", fallback=True
            )
            self.cache_expire = self.cfg_parser.getint(
                self.CONFIG_SECTION_HARVESTER, "cache_expire", fallback=86400
            )
            self.db_schema = self.cfg_parser.get(
                self.CONFIG_SECTION_HARVESTER, "db_schema", fallback="evucsk"
            )
            self.throttle_sleep = self.cfg_parser.getfloat(
                self.CONFIG_SECTION_HARVESTER, "throttle_sleep", fallback=0.25
            )

            # Prometheus - optional:
            self.prometheus_uri = self.cfg_parser.get(
                self.CONFIG_SECTION_PROMETHEUS, "uri", fallback=None
            )

            # requests library - optional:
            self.retry_connect = self.cfg_parser.getint(
                self.CONFIG_SECTION_REQUESTS, "retry_connect", fallback=3
            )
            self.retry_backoff = self.cfg_parser.getfloat(
                self.CONFIG_SECTION_REQUESTS, "retry_backoff", fallback=0.25
            )

            # SQLAlchemy - optional:
            self.sqlalchemy_echo = self.cfg_parser.getboolean(
                self.CONFIG_SECTION_SQLALCHEMY, "echo", fallback=False
            )
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            logger.error("%s (file %s)" % (e, self.CONFIG_FILE_NAME))
            sys.exit(-1)


common_config = CommonConfig()


Base = declarative_base(metadata=MetaData(schema=common_config.db_schema))


# see https://requests-cache.readthedocs.io/en/latest/user_guide.html#usage
def make_throttle_hook(timeout=1.0):
    def hook(response, *args, **kwargs):
        if not getattr(response, "from_cache", False):
            logger.debug("throttle hook: timeout: %s" % timeout)
            time.sleep(timeout)
        return response

    return hook


class CommonHarvesterResources:

    USE_FILE_ADAPTER = False

    def __init__(self, config):
        self.config = config

        # requests stuff:
        self.requests_session = requests.Session()
        if self.USE_FILE_ADAPTER:
            from requests_file import FileAdapter

            self.requests_session.mount("file://", FileAdapter())
        elif config.be_careful:
            # to avoid hitting source server too much,
            # we're going to cache all responses for configured amount of time
            self.requests_session = requests_cache.CachedSession(
                "evucsk_cache", expire_after=config.cache_expire
            )
            self.requests_session.hooks = {
                "response": make_throttle_hook(config.throttle_sleep)
            }
        retry = Retry(connect=config.retry_connect, backoff_factor=config.retry_backoff)
        adapter = HTTPAdapter(max_retries=retry)
        self.requests_session.mount("https://", adapter)

        # SQLAlchemy stuff:
        self.engine = create_engine(config.db_string, echo=config.sqlalchemy_echo)
        self.Session = sessionmaker(self.engine)
        self.session = self.Session()

        self.inspect = inspect(self.engine)

        self.metadata = Base.metadata
        self.metadata.bind = self.engine
        self.metadata.reflect(self.engine, schema=config.db_schema)


class HarvesterBase:

    CONFIG_SECTION = None

    EVUCSK_XML_PHYSICAL_ADDRESS = "PhysicalAddress"

    def __init__(self, cfg_parser, common_resources):
        self.common_resources = common_resources
        self.local_dataset_fn = "%s.xml" % self.CONFIG_SECTION
        self.git_repo_dir = self.git_repo = None

        try:
            # optional: exception for early processing
            self.harvester_enabled = cfg_parser.getboolean(
                self.CONFIG_SECTION, "enabled", fallback=True
            )
            if not self.harvester_enabled:
                return  # no need to continue

            # mandatory:
            self.url = cfg_parser.get(self.CONFIG_SECTION, "url")

            # optional:
            self.local_dataset_fn = cfg_parser.get(
                self.CONFIG_SECTION, "local_dataset_fn", fallback=self.local_dataset_fn
            )

            self.git_repo_dir = cfg_parser.get(
                self.CONFIG_SECTION, "git_repo", fallback=None
            )
            cfg_index_columns = cfg_parser.get(
                self.CONFIG_SECTION, "index_columns", fallback=None
            )
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            logger.error(e)
            sys.exit(-1)

        if self.git_repo_dir:
            self.git_repo = Repo(self.git_repo_dir)
        if cfg_index_columns:
            self.index_columns = cfg_index_columns.split(",")

    def _get_local_dataset_file(self):
        mode = "bw"

        if not self.git_repo:
            return tempfile.NamedTemporaryFile(prefix=self.local_dataset_fn, mode=mode)

        fn = os.path.join(self.git_repo_dir, self.local_dataset_fn)
        return open(fn, mode)

    def _handle_git_stuff(self):
        git = self.git_repo.git

        something_changed = False
        try:
            git.diff("--quiet")
        except GitCommandError:
            something_changed = True
        if not something_changed:
            return

        git.add(self.local_dataset_fn)
        commit_msg = "automatic update %s" % datetime.now().strftime("%Y%m%d-%H%M%S")
        git.commit("-m", commit_msg)
        logger.debug(
            "%s committed into %s: %s"
            % (self.git_repo_dir, self.local_dataset_fn, commit_msg)
        )

    def download_file(self):
        """
        Fetch the XML data and write it to the file.
        :returns: tuple (file name, xml data)
        """

        # get the file
        logger.info("fetching dataset: %s" % self.url)
        r = self.common_resources.requests_session.get(
            self.url,
            auth=(
                self.common_resources.config.evucsk_user,
                self.common_resources.config.evucsk_password,
            ),
        )
        if r.status_code != 200:
            raise RuntimeError(
                "dataset download failed, status code: %d" % r.status_code
            )

        # store the file (for now /tmp, later add option to push files into Git repo)
        f = self._get_local_dataset_file()
        with f:
            f.write(r.content)
        f.close()
        logger.debug("dataset written to %s" % f.name)

        if self.git_repo:
            self._handle_git_stuff()

        return (f.name, r.content)

    def parse_elements(self, xml_element, target, tags, mandatory=True):
        """
        Takes all elements from `xml_element` with tags given in `tags` (list) and
        add them into `target` (dictionary).
        :param mandatory: raise KeyError if a tag is not found in xml_element
        """
        for tag in tags:
            target_key = tag.lower()
            target_value = xml_element.find(tag)
            if target_value is None:
                if not mandatory:
                    # fill-in blanks as defaults, to make sure we always produce table with same structure/columns
                    target[target_key] = None
                    continue
                else:
                    raise KeyError(
                        "tag %s not found (element: %s)" % (tag, xml_element)
                    )
            target_value = target_value.text
            target[target_key] = target_value

    @staticmethod
    def convert_bool(str_value):
        if str_value is None:
            return None

        value = str_value.upper()
        if value == "TRUE":
            return True
        elif value == "FALSE":
            return False
        raise ValueError("invalid boolean: %s" % str_value)

    @staticmethod
    def convert_date(str_value):
        if str_value is None:
            return None

        # exception: sometimes values like 'Tue Mar 31 00:00:00 CEST 2020' are used ...
        if " " in str_value:
            # in case we're running on a system which has non-EN locale set
            original_locale = locale.setlocale(locale.LC_ALL, "C")

            # to make sure we run also on system which does not have info about CET/CEST time zones, see issue #6
            str_value = str_value.replace("CEST", "+02:00").replace("CET", "+01:00")
            result = datetime.strptime(str_value, "%a %b %d %H:%M:%S %z %Y").date()

            locale.setlocale(locale.LC_ALL, original_locale)
            return result

        return datetime.strptime(str_value, "%d.%m.%Y").date()

    @staticmethod
    def convert_float(str_value):
        if str_value is None:
            return None

        # note: sometime there is ',' instead of '.'
        return float(str_value.replace(",", "."))

    @staticmethod
    def convert_int(str_value):
        if not str_value:
            return None
        return int(str_value)

    @staticmethod
    def convert_time(str_value):
        if str_value is None:
            return None
        return datetime.strptime(str_value, "%H:%M").time()

    @staticmethod
    def convert_time_with_24handling(str_value):
        # outlier: "24:00" is not a valid time but in dataset it means
        # "midnight of that day". So, we use "23:59" instead. Thus
        # "from 00:00 to 23:59" means "whole day".
        if str_value == "24:00":
            return HarvesterBase.convert_time("0:00")
        else:
            return HarvesterBase.convert_time(str_value)

    # source: https://gist.github.com/chrisjsimpson/076a82b51e8540a117e8aa5e793d06ec
    @staticmethod
    def convert_dms2dec(dms_str):
        dms = re.sub(r"\s", "", dms_str)

        sign = -1 if re.search("[swSW]", dms) else 1

        numbers = [*filter(len, re.split("\D+", dms, maxsplit=4))]

        degree = numbers[0]
        minute = numbers[1] if len(numbers) >= 2 else "0"
        second = numbers[2] if len(numbers) >= 3 else "0"
        frac_seconds = numbers[3] if len(numbers) >= 4 else "0"

        second += "." + frac_seconds
        return sign * (int(degree) + float(minute) / 60 + float(second) / 3600)

    @staticmethod
    def convert_lat_lon(str_value):
        if str_value and "°" in str_value:
            return HarvesterBase.convert_dms2dec(str_value)
        return HarvesterBase.convert_float(str_value)

    @staticmethod
    def apply_conversion(data, keys, conv_func, mandatory=True):
        for key in keys:
            if key not in data:
                if mandatory:
                    raise ValueError("key %s not found in data: %s" % (key, data))
                else:
                    continue
            data[key] = conv_func(data[key])

    def parse_physical_address(self, xml_element, target, mandatory=True):
        """
        Takes all elements of 'PhysicalAddress' from `xml_element` and
        add them into `target` (dictionary).
        """

        # fill-in blanks as defaults, to make sure we always produce table with same structure/columns
        target["addressline"] = None
        target["municipality"] = None
        target["buildingname"] = None
        target["buildingnumber"] = None
        target["county"] = None
        target["propertyregistrationnumber"] = None
        target["streetname"] = None
        target["unit"] = None
        target["postalcode"] = None

        xml_ps = xml_element.find(self.EVUCSK_XML_PHYSICAL_ADDRESS)
        if xml_ps is None:
            if mandatory:
                raise KeyError(
                    "physical address (%s) not found in %s"
                    % (self.EVUCSK_XML_PHYSICAL_ADDRESS, xml_element)
                )
            else:
                return

        self.parse_elements(xml_ps, target, ["AddressLine"])
        self.parse_elements(
            xml_ps,
            target,
            [
                "BuildingName",
                "BuildingNumber",
                "County",
                "Municipality",
                "PropertyRegistrationNumber",
                "StreetName",
                "Unit",
            ],
            mandatory=False,
        )

        xml_da = xml_ps.find("DeliveryAddress")
        if xml_da:
            self.parse_elements(xml_da, target, ["PostalCode"], mandatory=False)
        else:
            target["postalcode"] = None

    def parse_kontakt(self, xml_element):
        kontakt = {}

        self.parse_elements(xml_element, kontakt, ["TYP", "TYP_LABEL"])
        # weird: 'kontakt' is sometimes filled but the main stuff, i.e. 'hodnota' is missing
        self.parse_elements(xml_element, kontakt, ["HODNOTA"], mandatory=False)

        return kontakt

    def parse_kontakty(self, xml_element, parent_id):
        """
        @param xml_element: look for contacts in this XML element
        @param parent_id: link contact items with this parent ID

        Note: 'parent_id' will be filed into field with name defined in 'self.KONTAKT_BACKREFERENCE_KEY'
        """
        # contacts (kontakty), optional
        # ... and we also "commit" those into the list already,
        # since passing them up the call chain to 'parse_row()' seems
        # more problematic then beneficial
        xml_kontakty = xml_element.find("KONTAKTY")
        if xml_kontakty:
            for xml_kontakt in xml_kontakty.findall("KONTAKT"):
                kontakt = self.parse_kontakt(xml_kontakt)
                kontakt[self.KONTAKT_BACKREFERENCE_KEY] = parent_id
                self.kontakt_list.append(kontakt)

    def parse_zastupca(self, xml_element):
        zastupca = {}
        zastupca["meno"] = xml_element.text

        return zastupca

    def parse_zastupcovia(self, xml_element, tag_name, subtag_name, parent_id):
        xml_zastupcovia = xml_element.find(tag_name)
        for xml_zastupca in xml_zastupcovia.findall(subtag_name):
            zastupca = self.parse_zastupca(xml_zastupca)
            zastupca[self.ZASTUPCA_BACKREFERENCE_KEY] = parent_id
            self.zastupcovia_list.append(zastupca)

    def store_df_kontakty(self, name):
        df_kontakt = DataFrame(self.kontakt_list)
        self.store_table(df_kontakt, name)
        self.kontakt_list = df_kontakt = None

    def store_df_zastupcovia(self, name):
        df_zastupcovia = DataFrame(self.zastupcovia_list)
        df_zastupcovia.rename_axis("id", inplace=True)
        df_zastupcovia.set_index(
            self.ZASTUPCA_BACKREFERENCE_KEY,
            append=True,
            inplace=True,
            verify_integrity=True,
        )
        self.store_table(df_zastupcovia, name)
        self.zastupcovia_list = df_zastupcovia = None

    def store_table(self, df, name, dtype=None):
        # To allow triggers to be defined for target table, we'd like to avoid
        # dropping that table. But drop is required by pandas in order to properly
        # propagate new data and delete old data. Thus we:

        # 0) does that table already exist?
        table_exists = self.common_resources.inspect.has_table(
            name, schema=self.common_resources.config.db_schema
        )
        delete_count = 0

        # 1) write panda frame to temporary table
        tmp_name = name + "_tmp"
        if not table_exists:
            tmp_name = name
        df.to_sql(
            tmp_name,
            self.common_resources.engine,
            schema=self.common_resources.config.db_schema,
            dtype=dtype,
        )
        self.common_resources.session.commit()
        logger.debug("%d items written to table %s" % (df.shape[0], tmp_name))

        if table_exists:
            # 2) delete data from target table
            table = self.common_resources.metadata.tables[
                self.common_resources.config.db_schema + "." + name
            ]
            query = sqlalchemy.delete(table)
            r = self.common_resources.session.execute(query)
            delete_count = r.rowcount
            logger.debug("%s items deleted from table %s" % (r.rowcount, name))

            # 3) copy data from temporary to target table
            tmp_table = sqlalchemy.Table(
                tmp_name,
                self.common_resources.metadata,
                autoload_with=self.common_resources.engine,
            )
            tmp_columns = []
            for column in table.columns:
                tmp_columns.append(tmp_table.c[column.name])
            query = sqlalchemy.insert(table).from_select(
                table.columns, sqlalchemy.select(*tuple(tmp_columns))
            )
            r = self.common_resources.session.execute(query)
            logger.debug(
                "%s items copied from table %s to table %s"
                % (r.rowcount, tmp_name, name)
            )
            self.common_resources.session.commit()

            # 4) delete temporary table
            tmp_table.drop(bind=self.common_resources.engine)

        logger.info(
            "%d new items written to table %s and deleted %d old items"
            % (df.shape[0], name, delete_count)
        )

    def run(self):
        if not self.harvester_enabled:
            logger.debug("harvester %s disabled" % self.CONFIG_SECTION)
            return

        (fn, xml_data) = self.download_file()
        self.process_dataset(xml_data)
        self.store_dataset()
