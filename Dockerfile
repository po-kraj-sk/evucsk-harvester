FROM python:3.11

LABEL project="georchestra.org"
LABEL org.opencontainers.image.authors="jeanpommier@pi-geosolutions.fr"

#install envsubst
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        gettext-base \
        ncat \
    && rm -rf /var/lib/apt/lists/*


# Add non-privileged user to run the script
RUN useradd -M -s /bin/bash -U harvester -u 999


# Copy the application
COPY --chown=harvester:harvester requirements.txt LICENSE.txt README.md logging_config* evucsk_harvester.* /app/
RUN pip install -r /app/requirements.txt
COPY --chown=harvester:harvester evucsk_harvester  /app/evucsk_harvester
WORKDIR /app


# Set up entrypoint. Will take care of the app's configuration
COPY --chown=harvester:harvester docker /
#COPY docker /
RUN chmod +x /entrypoint.sh &&\
    chmod +x /docker-entrypoint.d/*
ENTRYPOINT [ "/entrypoint.sh" ]


ENV EVUCSK_USERNAME="" \
    EVUCSK_PASSWORD="" \
    EVUCSK_ID="" \
    GIT_LOCAL_FOLDER="/tmp/evucsk" \
    EVUCSK_ENABLE_LEKARNE="True" \
    EVUCSK_ENABLE_POHOTOVOST="True" \
    EVUCSK_ENABLE_ZARIADENIA="True" \
    EVUCSK_ENABLE_SLUZBY="True" \
    DB_CONNECTION_STRING="" \
    DB_CONNECTION_STRING_FILE="" \
    DB_SCHEMA="statsoffice" \
    LOG_LEVEL="INFO" \
    BE_CAREFUL="True" \
    RETRY_CONNECT=3 \
    RETRY_BACKOFF=0.25 \
    LOG_FILE="/app/logs/harvesters/evucsk_harvester.log"

RUN mkdir -p /app/logs && chown harvester:harvester /app/logs
VOLUME /app/logs

# Run as Alpine's Guest user
USER harvester
# Actually no, we'll run this one as root, so that we can easily write to the volume (git). Maybe to improve later
CMD ["python", "-m", "evucsk_harvester"]
#CMD ["sleep", "10000"]
